#!/usr/bin/env escript
%% ex: ft=erlang ts=4 sw=4 et
%% Create a hex registry containing only entries for rebar3's dependencies.
main(_) ->
	application:start(ets),
	Table = ets:new(package_index, [public]),
	ets:insert(Table, [{'$$version$$', 4}]),
	ets:insert(Table, [{'$$installs2$$', []}]),

	ets:insert(Table, [{<<"bbmustache">>, [[<<"1.5.0">>]]}]),
	ets:insert(Table, [{{<<"bbmustache">>, <<"1.5.0">>}, [[], <<>>, [<<"rebar">>]]}]),

	ets:insert(Table, [{<<"certifi">>, [[<<"2.3.1">>]]}]),
	ets:insert(Table, [{{<<"certifi">>, <<"2.3.1">>}, [[], <<>>, [<<"rebar">>]]}]),

	ets:insert(Table, [{<<"cf">>, [[<<"0.3.1">>]]}]),
	ets:insert(Table, [{{<<"cf">>, <<"0.3.1">>}, [[], <<>>, [<<"rebar">>]]}]),

	ets:insert(Table, [{<<"cth_readable">>, [[<<"1.3.4">>]]}]),
	ets:insert(Table, [{{<<"cth_readable">>, <<"1.3.4">>}, [[], <<>>, [<<"rebar">>]]}]),

	ets:insert(Table, [{<<"erlware_commons">>, [[<<"1.0.4">>]]}]),
	ets:insert(Table, [{{<<"erlware_commons">>, <<"1.0.4">>}, [[], <<>>, [<<"rebar">>]]}]),

	ets:insert(Table, [{<<"eunit_formatters">>, [[<<"0.5.0">>]]}]),
	ets:insert(Table, [{{<<"eunit_formatters">>, <<"0.5.0">>}, [[], <<>>, [<<"rebar">>]]}]),

	ets:insert(Table, [{<<"getopt">>, [[<<"1.0.1">>]]}]),
	ets:insert(Table, [{{<<"getopt">>, <<"1.0.1">>}, [[], <<>>, [<<"rebar">>]]}]),

	ets:insert(Table, [{<<"providers">>, [[<<"1.7.0">>]]}]),
	ets:insert(Table, [{{<<"providers">>, <<"1.7.0">>}, [[], <<>>, [<<"rebar">>]]}]),

	ets:insert(Table, [{<<"relx">>, [[<<"3.24.5">>]]}]),
	ets:insert(Table, [{{<<"relx">>, <<"3.24.5">>}, [[], <<>>, [<<"rebar">>]]}]),

	ets:insert(Table, [{<<"ssl_verify_fun">>, [[<<"1.1.3">>]]}]),
	ets:insert(Table, [{{<<"ssl_verify_fun">>, <<"1.1.3">>}, [[], <<>>, [<<"rebar">>]]}]),
	Filename = "debian_registry.ets",
	ets:tab2file(Table, Filename).
