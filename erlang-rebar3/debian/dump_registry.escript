#!/usr/bin/env escript
%% ex: ft=erlang ts=4 sw=4 et
%% Dump content of an ETS file.
main(_) ->
	application:start(ets),
	{_, Table} = ets:file2tab("debian_registry.ets"),
	lists:foreach(fun(Entry) -> erlang:display(Entry) end, ets:match(Table, '$1')).
