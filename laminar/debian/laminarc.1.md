% laminarc(1) laminar client
% Oliver Giles
% March 19, 2018

# NAME

laminarc - laminar client

# SYNOPSIS

laminarc <command> [parameters...]

# DESCRIPTION

`laminarc` connects to `laminard` using the address supplied by the
`LAMINAR_HOST` environment variable. If it is not set, `laminarc` will first
attempt to use `LAMINAR_BIND_RPC,` which will be available if `laminarc` is
executed from a script within `laminard`. If neither `LAMINAR_HOST` nor
`LAMINAR_BIND_RPC` is set, `laminarc` will assume a default host of
`unix:/var/run/laminar/laminar.sock`.

# COMMANDS

`laminarc trigger [JOB [PARAMS...]]...`
:   triggers one or more jobs with optional parameters, returning immediately.

`laminarc start [JOB [PARAMS...]]...`
:   triggers one or more jobs with optional parameters and waits for the
    completion of all jobs. Returns a non-zero error code if any job failed.

`laminarc set [VARIABLE=VALUE]...`
:   sets one or more variables to be exported in subsequent scripts for the run
    identified by the `$JOB` and `$RUN` environment variables.

`laminarc lock [NAME]`
:   acquires the lock named `NAME`.

`laminarc release [NAME]`
:   releases the lock named `NAME`.

# SEE ALSO

`laminard` (1).
The *README* file distributed with laminar contains full documentation.

The laminar source code and all documentation may be downloaded from
<https://laminar.ohwg.net/>.

